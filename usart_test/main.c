#include "stm32f4xx.h"                 
#include "stm32f4xx_usart.h"           
#include "stm32f4xx_gpio.h"           
#include "stm32f4xx_rcc.h"    
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


void USART_Config(void)
{
	// Enable clock for GPIOB
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
  // Enable clock for USART1
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  // Connect PB6 to USART1_Tx
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);
	
	// Connect PB7 to USART1_Rx
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);

	// Initialization of GPIOB
	GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_Init(GPIOB, &GPIO_InitStruct);

	// Initialization of USART1
  USART_InitTypeDef USART_InitStruct;
  USART_InitStruct.USART_BaudRate = 9600;
  USART_InitStruct.USART_HardwareFlowControl =USART_HardwareFlowControl_None;
  USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
  USART_InitStruct.USART_Parity = USART_Parity_No;
  USART_InitStruct.USART_StopBits = USART_StopBits_1;
  USART_InitStruct.USART_WordLength = USART_WordLength_8b;
	USART_Init(USART1, &USART_InitStruct);

	// Enable USART1
  USART_Cmd(USART1, ENABLE);
}

void USART_PutChar(char c)
{
	 while (!USART_GetFlagStatus(USART1, USART_FLAG_TXE));
	 USART_SendData(USART1, c);
}

void USART_PutString(char *s)
{
	while (*s)
	{
		USART_PutChar(*s++);
	}
}

uint16_t USART_GetChar()
{
	while (!USART_GetFlagStatus(USART1, USART_FLAG_RXNE));
	return USART_ReceiveData(USART1);
}

	char *par[2];
	char *alinan;
	int i;
	int x;
	int y;
int main(void)
{
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	GPIO_InitTypeDef GPIO_InitDef;
	GPIO_InitDef.GPIO_Pin = GPIO_Pin_12 |GPIO_Pin_13 |GPIO_Pin_14 |GPIO_Pin_15;
	GPIO_InitDef.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitDef.GPIO_OType = GPIO_OType_PP;
	GPIO_InitDef.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitDef.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitDef);
	
	USART_Config();
	USART_PutString("Hello, World!\n");
	
	while (1)
		{
			// Get a char from PC
			uint16_t data = USART_GetChar();
			char *stringData = (char*) &data;
			
			alinan = strtok(stringData,"-");
			i=0;
			while (alinan != NULL)
				{
					par[i++]=alinan;
					alinan = strtok (NULL, "-"); //alinan veriyi "-" gelince par�ala
				}
				x=(int) &par[0];
				y=atoi(par[1]);
				USART_PutString(par[0]);
				
			
			if (x == 150 && y == 250)
				{ 
					GPIO_SetBits(GPIOD, GPIO_Pin_13);
        }
			else if (x == 350 && y == 450)
				{
					GPIO_ResetBits(GPIOD, GPIO_Pin_13);
        }
				else if(i==1)
				{
					i=0;
				}
    }

}



